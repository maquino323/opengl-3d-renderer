#version 440 core
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

// input from tess eval shader
in ES_OUT
{
	vec3 pos;
	vec3 normal;
	vec2 texcoords;
	vec3 tangent;
	vec3 bitangent;

} gs_in[];

// output to fragment shader
out vec3 v_Pos;
out vec3 v_Normal;
out vec2 v_TexCoords;
out vec3 vTangent;
out vec3 vBitangent;

void main(void)
{
	gl_Position = gl_in[0].gl_Position;
	v_Pos = gs_in[0].pos;
	v_Normal = gs_in[0].normal;
	v_TexCoords = gs_in[0].texcoords;
	vTangent = gs_in[0].tangent;
	vBitangent = gs_in[0].bitangent;
	EmitVertex();

	gl_Position = gl_in[1].gl_Position;
	v_Pos = gs_in[1].pos;
	v_Normal = gs_in[1].normal;
	v_TexCoords = gs_in[1].texcoords;
	vTangent = gs_in[1].tangent;
	vBitangent = gs_in[1].bitangent;
	EmitVertex();

	gl_Position = gl_in[2].gl_Position;
	v_Pos = gs_in[2].pos;
	v_Normal = gs_in[2].normal;
	v_TexCoords = gs_in[2].texcoords;
	vTangent = gs_in[2].tangent;
	vBitangent = gs_in[2].bitangent;
	EmitVertex();

	EndPrimitive();
}