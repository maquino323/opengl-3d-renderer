#version 460 core

layout (vertices = 3) out;

uniform vec3 cameraPosition;

// input from vertex shader
in VS_OUT
{
  vec3 vPos;
  vec3 vNormal;
  vec2 vTexCoords;

} cs_in[];

// output to tess eval shader
out CS_OUT
{
  vec3 vPos;
  vec3 vNormal;
  vec2 vTexCoords;

} cs_out[];

// get tess level based on camera distance to vertex position
float GetTessLevel(float dist0, float dist1)
{
	float avgDistance = (dist0 + dist1) / 2.0;
	
	// from highest level to lowest
	if (avgDistance <= 2.0)
		return 10.0;

	else if (avgDistance <= 5.0)
		return 7.0;

	else
		return 3.0;
}

void main()
{
	// pass along data to evaluation shader
	cs_out[gl_InvocationID].vPos = cs_in[gl_InvocationID].vPos;
	cs_out[gl_InvocationID].vNormal = cs_in[gl_InvocationID].vNormal;
	cs_out[gl_InvocationID].vTexCoords = cs_in[gl_InvocationID].vTexCoords;

	// calculate vertex distance for tessellation levels
	float eyeToVertexDistance0 = distance(cameraPosition, cs_in[0].vPos);
	float eyeToVertexDistance1 = distance(cameraPosition, cs_in[1].vPos);
	float eyeToVertexDistance2 = distance(cameraPosition, cs_in[2].vPos);

	gl_TessLevelOuter[0] = GetTessLevel(eyeToVertexDistance1, eyeToVertexDistance2);
	gl_TessLevelOuter[1] = GetTessLevel(eyeToVertexDistance2, eyeToVertexDistance0);
	gl_TessLevelOuter[2] = GetTessLevel(eyeToVertexDistance0, eyeToVertexDistance1);
	gl_TessLevelInner[0] = gl_TessLevelOuter[2];
}