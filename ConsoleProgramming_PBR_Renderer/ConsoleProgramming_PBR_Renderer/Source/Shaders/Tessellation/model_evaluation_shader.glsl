#version 460 core

layout(triangles, equal_spacing, ccw) in;

uniform mat4 viewProjection;
uniform float displacementFactor;
uniform sampler2D displacementMap;

// input from tess control shader
in CS_OUT
{
  vec3 vPos;
  vec3 vNormal;
  vec2 vTexCoords;

} es_in[];

// output to geometry shader
out ES_OUT
{
  vec3 pos;
  vec3 normal;
  vec2 texcoords;
  vec3 tangent;
  vec3 bitangent;

} es_out;

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
	return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)
{
	return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

void main()
{
	es_out.texcoords = interpolate2D(es_in[0].vTexCoords, es_in[1].vTexCoords, es_in[2].vTexCoords);
	es_out.normal = interpolate3D(es_in[0].vNormal, es_in[1].vNormal, es_in[2].vNormal);
	es_out.normal = normalize(es_out.normal);
	es_out.pos = interpolate3D(es_in[0].vPos, es_in[1].vPos, es_in[2].vPos);

	vec3 edge1 = vec3(es_in[1].vPos) - vec3(es_in[0].vPos);	
	vec3 edge2 = vec3(es_in[2].vPos) - vec3(es_in[0].vPos);
	
	vec2 deltaUV1 = es_in[1].vTexCoords - es_in[0].vTexCoords;
	vec2 deltaUV2 = es_in[2].vTexCoords - es_in[0].vTexCoords;

	float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

	es_out.tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
	es_out.tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
	es_out.tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);


	es_out.bitangent.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
	es_out.bitangent.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
	es_out.bitangent.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);


	float disp = texture(displacementMap, es_out.texcoords).x;
	//disp += texture(displacementMap, es_out.texcoords).y;
	es_out.pos += es_out.normal * disp * displacementFactor;
	gl_Position = viewProjection * vec4(es_out.pos, 1.0);
	//gl_Position = vec4(es_out.pos, 1.0);
}