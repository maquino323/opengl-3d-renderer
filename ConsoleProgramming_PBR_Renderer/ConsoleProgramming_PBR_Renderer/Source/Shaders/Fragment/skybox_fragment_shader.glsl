#version 330 core

out vec4 FragColor;
in vec3 WorldPos;

uniform samplerCube envMap;

void main()
{
	float gamma = 2.2;
	vec3 envColor = texture(envMap, WorldPos).rgb;

	// tone mapping
	envColor = envColor / (envColor + vec3(1.0));
	envColor = pow(envColor, vec3(1.0 / gamma));

	FragColor = vec4(envColor, 1.0);
}