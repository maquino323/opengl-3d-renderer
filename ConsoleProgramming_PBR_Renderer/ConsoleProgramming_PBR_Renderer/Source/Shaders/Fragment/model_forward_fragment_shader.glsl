#version 460 core

in vec3 v_Pos;
in vec3 v_Normal;
in vec2 v_TexCoords;
in vec3 vTangent;
in vec3 vBitangent;

// light values
uniform vec3 lightColors[5];
uniform vec3 lightPositions[5];
uniform float lightIntensities[5];
uniform float lightRadii[5];

uniform vec3 cameraPosition;

uniform sampler2D texture_diffuse;

out vec4 fragColor;


float calculateAttenuation(vec3 fragPosition, vec3 lightPosition, float radius)
{
    float attenuation = 0.0f;
    float dist = distance(lightPosition, fragPosition);
    float d = max(dist - radius, 0);

    float denom = d / radius + 1;

    attenuation = 1.0f / (denom * denom); // inverse square falloff, avoid div by 0

    return attenuation;
}

void main()
{
	// calculate lighting
	vec3 lightTotal = vec3(0.0f); 
	vec3 viewDir = normalize(cameraPosition - v_Pos);
	vec3 albedo = texture(texture_diffuse, v_TexCoords).rgb;

	for (int i = 0; i < 5; ++i)
	{
		float attenuation = calculateAttenuation(v_Pos, lightPositions[i], lightRadii[i]);
		vec3 lightDir = normalize(lightPositions[i] - v_Pos);
		vec3 diffuse = max(dot(v_Normal, lightDir), 0.0) * lightColors[i] * lightIntensities[i] * attenuation;
		lightTotal += diffuse;
	}

	fragColor = vec4(albedo, 1.0) + vec4(lightTotal, 1.0);
}