#version 460 core

in vec2 vTexcoords;

#define MAX_NUMBER_OF_LIGHTS 500

// light values
uniform vec3 lightColors[MAX_NUMBER_OF_LIGHTS];
uniform vec3 lightPositions[MAX_NUMBER_OF_LIGHTS];
uniform float lightIntensities[MAX_NUMBER_OF_LIGHTS];
uniform float lightRadii[MAX_NUMBER_OF_LIGHTS];

uniform vec3 cameraPosition;
uniform bool useTonemapping;

// g buffer samplers
uniform sampler2D gPosition;
uniform sampler2D gNormals;
uniform sampler2D gAlbedo;

uniform float numLights;

layout (location = 0) out vec4 composite;
//out vec4 fragColor;


float calculateAttenuation(vec3 fragPosition, vec3 lightPosition)
{
	float constant  = 1.0f;
	float linear = 2.0f;
	float quadratic = 0.3f;

    float attenuation = 0.0f;
    float dist = length(lightPosition - fragPosition);

	attenuation = 1.0f / (dist * dist);
	//attenuation = 1.0f / (constant + (linear * dist) + (quadratic * (dist * dist)));

    return attenuation;
}

void main()
{
	// retrieve data from g buffer
	vec3 fragPos = texture(gPosition, vTexcoords).rgb;		// fragment positions
	vec3 normal = texture(gNormals, vTexcoords).rgb;		// fragment normals
	vec3 albedo = texture(gAlbedo, vTexcoords).rgb;			// room texture
	//float intensity = texture(gSpecular, vTexcoords).a;

	// calculate lighting
	vec3 lightTotal = vec3(0.0f);

	vec3 viewDir = normalize(cameraPosition - fragPos);

	for (int i = 0; i < numLights; ++i)
	{
		vec3 lightCol = lightColors[i] * lightIntensities[i];
		vec3 lightDir = normalize(lightPositions[i] - fragPos);

		float attenuation = calculateAttenuation(fragPos, lightPositions[i]);

		//vec3 diffuse = max(dot(normal, lightDir), 0.0) * lightColors[i] * lightIntensities[i] * attenuation;
		vec3 diffuse = max(dot(normal, lightDir), 0.0) * lightCol * attenuation;
	
		lightTotal += diffuse;
	}
	

	vec4 result = vec4(albedo, 1.0) + vec4(lightTotal, 1.0);

	if (useTonemapping == true)
	{
		// reinhard tone mapping
		vec3 tonemap = result.rgb / (result.rgb + vec3(1.0));

		tonemap.rgb = pow(tonemap, vec3(1.0/2.2)); // gamma correction

		composite = vec4(tonemap, 1.0);
	}

	else
		composite = result;
			
}