#version 440 core

in vec3 vPos;
in vec3 vNormal;
in vec2 vTexcoords;

out vec4 fragColor;

uniform vec3 lightSource;
uniform vec4 lightColor;
uniform vec3 viewPosition;

uniform float time;


float ambientStrength = 0.1f;
float specularStrength = 0.5f;
int specularPower = 16;

struct Material
{
	sampler2D diffuseMap;
	sampler2D specularMap;
	float shininess;
};

uniform Material material;

void main()
{
	vec3 lightDirection = normalize(lightSource - vPos);
	vec3 viewDirection = normalize(viewPosition - vPos);
	vec3 reflectDirection = reflect(-lightDirection, vNormal);


	// lighting
	float lightIntensity = max(dot(lightDirection, normalize(vNormal)), 0.0f);

	//vec4 diffuse = (lightColor * dotProduct) * texture(material.diffuseMap, vTexcoords);
	vec4 diffuse =  texture(material.diffuseMap, vTexcoords) * lightIntensity;
	vec4 ambient = (ambientStrength * lightColor);
	float spec = pow(max(dot(viewDirection, reflectDirection), 0.0f), material.shininess);
	vec4 specular = spec * texture(material.specularMap, vTexcoords);

	//fragColor = (diffuse + ambient + specular);
	fragColor = diffuse;
	//fragColor = specular;
	fragColor.a = 1.0f; // makes sure alpha is always 1
}