#version 330 core

uniform vec3 color;
uniform bool useTonemapping;
out vec4 FragColor; 


void main()
{
	if (useTonemapping)
	{
		vec3 tonemap = color/ (color.rgb + vec3(1.0));
		tonemap = pow(tonemap, vec3(1.0 / 2.2));
		FragColor = vec4(tonemap, 1.0);
	}

	else
		FragColor = vec4(color, 1.0);
}