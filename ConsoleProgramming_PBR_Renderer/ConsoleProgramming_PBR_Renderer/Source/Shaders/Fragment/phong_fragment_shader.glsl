#version 330 core
in vec3 WorldPos;
in vec3 Normal; // vertex normal

uniform vec3 lightPosition;
uniform vec3 lightColor;
uniform vec4 Color; // object color
uniform vec3 ViewPos; // camera position

out vec4 FragColor;

void main()
{
	float ambientStrength = 0.2;
	float specularStrength = 2.0;

	vec3 ambience = vec3(0.0);
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	// ambient light
	ambience = ambientStrength * lightColor * vec3(Color);

	// diffuse light
	vec3 lightDirection = normalize(lightPosition - WorldPos);
	float lightAngle = max(dot(normalize(Normal), lightDirection), 0.0);

	diffuse = lightColor.rgb * lightAngle;

	// specular light
	vec3 viewDir = normalize(ViewPos - WorldPos);
	vec3 reflectDir = reflect(-lightDirection, Normal); 

	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 256);
	specular = specularStrength * spec * lightColor; 

	FragColor = vec4(ambience + diffuse + specular, 1.0) * Color;
	//FragColor = vec4(ambience, 1.0);// + Color;
	//FragColor = vec4(diffuse, 1.0);// + Color;
	//FragColor = vec4(ambience + diffuse, 1.0) * Color;
	//FragColor = vec4(specular, 1.0) + Color;
	FragColor.a = 1.0;
}