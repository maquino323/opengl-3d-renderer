#version 460 core

in vec2 vTexcoords;

uniform sampler2D screenTexture;

out vec4 fragColor;

void main()
{
	fragColor = texture(screenTexture, vTexcoords);	// sample from render target
	fragColor.a = 1.0f;
	//fragColor = vec4(0.0, 1.0, 1.0, 1.0); // dummy output
}