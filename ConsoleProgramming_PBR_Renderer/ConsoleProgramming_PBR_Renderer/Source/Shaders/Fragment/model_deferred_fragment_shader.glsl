#version 440 core
in vec3 vPos;
in vec3 vNormal;
in vec2 vTexCoords;

uniform vec3 cameraPosition;
uniform vec3 viewDirection;

uniform sampler2D texture_diffuse;
uniform sampler2D texture_normal;

// render targets
layout (location = 0) out vec4 color;
layout (location = 1) out vec4 positions;
layout (location = 2) out vec4 normals;
layout (location = 4) out vec4 depth;


void main()
{   
    // textures
    vec3 diffuse = texture(texture_diffuse, vTexCoords).rgb; // albedo
    vec3 spec = texture(texture_diffuse, vTexCoords).rgb;    // specular highlight

    // fill render targets
    color       = vec4(diffuse, 1.0);       
    normals     = vec4(vNormal, 1.0);      
    positions   = vec4(vPos, 1.0);       

    
    // calculate depth
    float z = gl_FragCoord.z * 2.0 - 1.0; // back to NDC
    float d = (2.0f * 0.1f * 1000.0f) / (1000.0f + 0.1f - z * (1000.0f - 0.1f)); // linearize depth

    d /= 1000.0f; 
    depth = vec4(vec3(d), 1.0);
}