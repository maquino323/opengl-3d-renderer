#version 460 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexcoords;

out vec3 vPos;
out vec2 vTexcoords;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	gl_Position = projection * view * model * vec4(aPos, 1.0);
	vPos = aPos;
	vTexcoords = aTexcoords;
}	