#version 440 core

layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

 
// output to tess control shader
out VS_OUT
{
	vec3 vPos;
	vec3 vNormal;
	vec2 vTexCoords;

} vs_out;

// uniform matrices
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	vs_out.vPos = vec3(model * vec4(aPos, 1.0));
	vs_out.vNormal = vec3(view * model * vec4(aNormal, 0.0));
	vs_out.vTexCoords = aTexCoords;


	gl_Position = projection * view * model * vec4(aPos, 1.0);
}