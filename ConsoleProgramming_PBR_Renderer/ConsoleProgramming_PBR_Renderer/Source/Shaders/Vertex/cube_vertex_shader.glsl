#version 440 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexcoords;

out vec3 vPos;
out vec3 vNormal;
out vec2 vTexcoords;


uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main()
{
	vPos = vec3(model * vec4(aPos, 1.0));
	vNormal = aNormal;
	vTexcoords = aTexcoords;
	gl_Position = projection * view * model * vec4(aPos, 1.0f);
	
}

