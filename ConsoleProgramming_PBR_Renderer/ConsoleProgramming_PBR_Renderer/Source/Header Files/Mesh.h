#ifndef MESH_H
#define MESH_H

#include "Shader.h"
#include "glm/matrix.hpp"
#include "glm/glm.hpp"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

using namespace std;
using namespace glm;


struct Vertex {
    vec3 Position;
    vec3 Normal;
    vec2 TexCoords;
	vec3 Tangent;
	vec3 BiTangent;
};


struct Texture
{
	unsigned int id;
	string type;
	string path;
};

class Mesh
{	
public:
	vector<Vertex> Vertices;
	vector<unsigned int> Indices;
	vector<Texture> Textures;
	unsigned int VAO;
	Mesh();
	Mesh(const vector<Vertex>& vert, const vector<unsigned int>& in, const vector<Texture>& text);
	void Draw(const Shader& shader);

private:
	unsigned int VBO, EBO;
	void setUpMesh();

};


#endif // !MESH_H