#ifndef _TEXTURE_H
#define _TEXTURE_H



#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;

class ObjectTexture
{
public:
	
	//constructors
	ObjectTexture();
	ObjectTexture(string imageName, bool flipImage);

	// destructor
	~ObjectTexture();

	// accessors
	string GetImageName();
	unsigned int GetID();


	// mutators
	unsigned int loadImage(string newImage, bool flipImage);
	void ChangeID(unsigned int newID);

private:

	string image;
	unsigned int ID;

};


#endif //_HDRTEXTURE_H