#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <string>
#include <iostream>
#include "glm/glm.hpp"
#include "Shader.h"
#include "Mesh.h"


#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
using namespace std;
using namespace glm;

//using namespace objl;
//#include "../Source/Header Files/OBJ_Loader.h"

unsigned int TextureFromFile(const char* path, const string& directory, bool gamma = false);



class Model
{
public:
	Model();
	Model(const string& path, bool gamma);
	void Draw(const Shader& shader);

	vector<Texture> textures_loaded;
	vector<Mesh> meshes;
	string directory;

	bool gammaCorrection;
	bool isLoaded;

private:

	void LoadModel(const string& path);
	void ProcessNode(aiNode* node, const aiScene* scene);
	Mesh ProcessMesh(aiMesh* mesh, const aiScene* scene);
	vector<Texture> LoadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);
};


#endif // !MODEL_H
