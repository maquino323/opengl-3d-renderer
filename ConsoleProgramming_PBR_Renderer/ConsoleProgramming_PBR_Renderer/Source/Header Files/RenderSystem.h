#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H


#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Texture.h"
#include "Light.h"

#include <iostream>
#include <string>
#include <map>

#include "GL/glew.h"
#include "GLFW/glfw3.h"

#include <ft2build.h>
#include FT_FREETYPE_H


// PATHS
#define VERTEX			"./Source/Shaders/Vertex/"
#define FRAGMENT		"./Source/Shaders/Fragment/"
#define GEOMETRY		"./Source/Shaders/Geometry/"
#define TESSELLATION	"./Source/Shaders/Tessellation/"
#define FONTS			"./Fonts/"
#define MODELS			"./OBJs/"
#define TEXTURES		"./Textures/"

class RenderSystem
{
public:

	RenderSystem();   // default constructor

	~RenderSystem();  // destructor

	int render(float sceneTimer);     // render scene

	void updateMembers(float sceneTime);

	// accessors
	int  getScreenWidth();
	int  getScreenHeight();
	Camera getCamera();

	// mutators
	//static void updateScreenSize(int width, int height);
	

	// variables
	GLFWwindow* newWindow;

	//static int screen_width;
	//static int screen_height;
	int screen_width;
	int screen_height;

	bool drawText = true;
	bool useSkybox = true;
	int randomSphereCount = 0;
	int spheresAddedToList = 0;

private:

	// ====================== MEMBER VARIABLES ========================================

// window size
	
	friend class InputManager;

	mat4 model, view, projection, mvp, textProj;

	float appTimer;

	// cube vertex data
	float cubeVertexAttributes[288] = // 8 floats per vertex, 6 vertices per face, 6 faces = 288 floats
	{ 
		// back face
	//   position             normal              texcoords
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		 1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
		 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
		// front face
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		 1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
		 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		// left face
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		// right face
		 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		 1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
		 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		 1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
		// bottom face
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		 1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
		 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		// top face
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		 1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		 1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
		 1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
	};

	float quadVertices[24] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
		// positions   // texCoords
		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		 1.0f, -1.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		 1.0f, -1.0f,  1.0f, 0.0f,
		 1.0f,  1.0f,  1.0f, 1.0f
	};

	float planeVertices[48] = 
	{
		 // positions		  // normals		   // texcoords
		-10.0f,  0.0f, -10.0f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
		 10.0f,  0.0f , 10.0f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
		 10.0f,  0.0f, -10.0f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
		 10.0f,  0.0f,  10.0f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
		-10.0f,  0.0f, -10.0f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
		-10.0f,  0.0f,  10.0f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f
	};

	Camera sceneCamera;

	// textures
	enum TextureNames
	{
		ICE_LAKE_ENV = 0,
		ICE_LAKE_REF = 1,
		GRAND_CANYON_ENV = 2,
		GRAND_CANYON_REF = 3,
		DESERT_ENV = 4,
		DESERT_REF = 5,		// add more textures after here if needed
		MAX_TEXTURE_COUNT,
	};

	string Textures[MAX_TEXTURE_COUNT]
	{
		// path for HDR images
		"Images/Ice_Lake_Env.hdr",
		"Images/Ice_Lake_Ref.hdr",
		"Images/GCanyon_C_YumaPoint_Env.hdr",
		"Images/GCanyon_C_YumaPoint_3k.hdr",
		"Images/Road_to_MonumentValley_Env.hdr",
		"Images/Road_to_MonumentValley_Ref.hdr"
	};
	
	// we only need 1 HDR Texture at a time
	ObjectTexture skyboxTexture;
	ObjectTexture cubeTexture, specularTexture;
	ObjectTexture modelTexture, modelSpecularMap, modelNormalMap, modelDispMap;
	ObjectTexture planeTexture;
	
	// colors
	vec3 colors[10] =
	{
		vec3(1.0, 0.0, 0.0),  // red		
		vec3(0.0, 1.0, 0.0),  // green	
		vec3(0.0, 0.0, 1.0),  // blue		
		vec3(0.0, 1.0, 1.0),  // cyan		
		vec3(1.0, 0.0, 1.0),  // magenta	
		vec3(1.0, 1.0, 0.0),  // yellow	
		vec3(1.0, 0.5, 0.0),  // orange	
		vec3(0.0, 0.5, 1.0),  // skyblue	
		vec3(0.0, 0.0, 0.0),  // black	
		vec3(1.0, 1.0, 1.0),  // white	
	};

	// objects
	unsigned int cubeVAO = 0, cubeVBO = 0;
	unsigned int sphereVAO = 0, sphereVBO = 0;
	unsigned int indexCount;
	unsigned int fsqVAO, fsqVBO;
	unsigned int planeVAO, planeVBO, planeEBO;

	int nrRows = 7;
	int nrColumns = 7;
	float spacing = 2.5;

	// Model
	bool modelLoaded = false;
	bool drawLights = false;
	bool drawWireFrame = false;
	bool shadersReloading = false;
	bool useTonemapping = false;

	Model loadedModel;

	string modelPaths[3] = 
	{
		"OBJs/suzuki/Srad 750.obj",
		"OBJs/nanosuit/nanosuit.obj",
		"OBJs/RangeRover/Range Rover Evoque.obj"
	};

	int currentModel = 0;

	GLuint VertexArrayID = 0;
	GLuint vertexBuffer = 1;
	GLuint colorBuffer = 2;
	GLuint vertexShaderID = 0, fragmentShaderID = 0;
	GLint maxPatchVertices;

	// Text
	unsigned int textVAO = 0, textVBO = 0;
	FT_Library ft;
	FT_Face face;
	FT_GlyphSlot g;

	struct Character
	{
		GLuint TextureID;
		ivec2 Size;
		ivec2 Bearing;
		FT_Pos Advance;
	};

	map<GLchar, Character> Characters;


	// Shaders
	Shader sphereShader;
	Shader lightShader;
	Shader textShader;
	Shader modelDeferredShader;
	Shader modelForwardShader;
	Shader cubeShader;
	Shader quadShader;
	Shader deferredLightingShader;
	Shader planeShader;

	// cubemap variables
	unsigned int captureFBO, captureRBO; // framebuffer object, render buffer object
	unsigned int envCubeMap, irradianceCubeMap;
	unsigned int lightUBO;

	// rendering
	GLuint sceneFramebuffer, sceneDepth, target;
	GLuint sceneCompositeBuffer, compositeAttachment;

	int numberOfLights[10] = { 2, 4, 8, 12, 25, 50, 100, 200, 400, 500 };
	int num = 0;

	GLuint renderTargets[5];

	int renderTarget = 3;
	int scene = 2;

	// set up projection and view matrices for capturing data on cubemap
	mat4 captureProjection = perspective(radians(90.0f), 1.0f, 0.1f, 10.0f);

	// capture views for cubemap
	mat4 captureView[6] =
	{
		lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(1.0f,  0.0f,  0.0f),  vec3(0.0f, -1.0f,  0.0f)),
		lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(-1.0f,  0.0f,  0.0f), vec3(0.0f, -1.0f,  0.0f)),
		lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f,  1.0f,  0.0f),  vec3(0.0f,  0.0f,  1.0f)),
		lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, -1.0f,  0.0f),  vec3(0.0f,  0.0f, -1.0f)),
		lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f,  0.0f,  1.0f),  vec3(0.0f, -1.0f,  0.0f)),
		lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f,  0.0f, -1.0f),  vec3(0.0f, -1.0f,  0.0f))
	};

	// Lights
	Light forwardlights[5];
	Light deferredLights[500];  // create an array of the maximum number of lights
	//Light deferredLights[4];

	// ======================= MEMBER FUNCTIONS =======================================

	void createCubemap(GLuint* texture, int x, int y);

	void DrawSphere();
	
	void DrawCube();
	
	void RenderControls();
	
	void DrawText(Shader& shader, string text, float startX, float startY, float scale, vec3 textColor);
	
	void ChangeBackgroundImage(unsigned int textureID);
	
	void createFramebuffer();
	
	void generateVertexObjects();

	void initTextLibrary();

	void setGraphicsSettings();

	void createShaders();

	void createGLFWContext();

	void createScene();

	void createTextures();

	void reloadShaders();
};


#endif // !RENDERSYSTEM_H