#ifndef APPMANAGER_H
#define APPMANAGER_H

#include "RenderSystem.h"
#include "InputManager.h"

class AppManager
{
public:

	AppManager();
	~AppManager();

	void initializeProgram();
	
	void runMainLoop();

	void update();

	bool pauseTimer = false;

private:

	float appTimer, currentTime, deltaTime, lastTime, frames;
	RenderSystem* appRenderSystem;
	InputManager* appInputManager;

};

#endif // !APPMANAGER_H