#ifndef _SHADER_H
#define _SHADER_H

#include "GL/glew.h"
#include "glm/matrix.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;
using namespace glm;

class Shader
{
public:

	unsigned int shaderID;

	// constructor
	Shader();
	Shader(const char* vertexShaderPath, const char* fragmentShaderPath);
	Shader(const char* vertexShaderPath, const char* geometryShaderPath, const char* fragmentShaderPath);
	Shader(const char* vertexShaderPath, const char* geometryShaderPath, const char* tessControlShaderPath, const char* tessEvalShaderPath, const char* fragmentShaderPath);

	// destructor
	~Shader();

	// activate the shader
	void use();

	// utility uniform functions
	void setBool(const string& name, bool value) const;
	void setInt(const string& name, int value) const;
	void setFloat(const string& name, float value) const;
	void setMat4(const string& name, mat4 matrix) const;
	void setVec3(const string& name, vec3 vect) const;
	void setVec4(const string& name, vec4 vect) const;

	void reloadShaders();


private:
	void checkCompileErrors(unsigned int shader, std::string type);
	const char* vsPath; 
	const char* tePath; 
	const char* tcPath; 
	const char* gsPath; 
	const char* fsPath;
	unsigned int v, te, tc, g, f;
};


#endif // _SHADER_H