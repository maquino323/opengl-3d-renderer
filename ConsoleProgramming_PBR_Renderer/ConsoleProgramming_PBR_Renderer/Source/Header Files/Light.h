#ifndef _LIGHT_H
#define _LIGHT_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/transform.hpp"

using namespace glm;
using namespace std;

class Light
{
	public:
		Light();
		Light(vec3 position, vec4 color, float intensity, float radius); // for point lights

		~Light();

		// mutators
		void SetLightPosition(vec3 newPos);
		void SetLightColor(vec4 newColor);
		void SetLightIntensity(float newIntensity);
		void SetRadius(float newRadius);
		vec4 ToggleLight(bool on);

		// accessors
		vec3 GetLightPosition();
		vec4 GetLightColor();
		float GetLightIntensity();
		float GetRadius();

	private:
		vec3 Position;
		vec4 Color;
		float Intensity;
		float Radius;
};


#endif // _LIGHT_H