#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include "RenderSystem.h"

class InputManager
{
public:

	InputManager();
	InputManager(RenderSystem* _renderSystem);
	
	~InputManager();

	void getInput(GLFWwindow* window, float deltaTime, bool &pause);
	

	bool exitProgram;


	//float getLastXPos();
	//float getLastYPos();

	//void setXPos(float newXPos);
	//void setYPos(float newYPos);

private:

	RenderSystem* renderSystem;

	//static float xOffset, yOffset;
	//static float lastXPos, lastYPos;
	//static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
	//static void mouse_callback(GLFWwindow* window, double xPos, double yPos);
};


#endif // !INPUTMANAGER_H