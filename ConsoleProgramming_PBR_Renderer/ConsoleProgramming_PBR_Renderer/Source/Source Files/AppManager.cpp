#include "AppManager.h"

using namespace std;


AppManager::AppManager()
{
	appTimer = 0.0f;
	currentTime = 0.0f;
	deltaTime = 0.0f;
	lastTime = 0.0f;
	frames = 0.0f;
};


AppManager::~AppManager()
{
	delete appInputManager;
	delete appRenderSystem;
	glfwTerminate();
};

void AppManager::initializeProgram()
{
	appRenderSystem = new RenderSystem();
	appInputManager = new InputManager(appRenderSystem);
}

void AppManager::runMainLoop()
{
	// grab input from Input Manager
	// update all actors in scene
	// render new frame
	while (!appInputManager->exitProgram)
	{
		currentTime = glfwGetTime();
		frames++;
		deltaTime = currentTime - lastTime;
		lastTime = currentTime;

		//cout << "\rApp Time: " << appTimer;
		cout << "\rDelta Time (ms): " << deltaTime * 1000.0f;
		//cout << "\rDelta Time (ms): " << deltaTime;

		//printf("\rRandom Sphere Count: %i  |  Number of spheres actually added: %i", appRenderSystem->randomSphereCount, appRenderSystem->spheresAddedToList);

		//lastTime = currentTime;
		if (pauseTimer == false)
			appTimer += deltaTime;
		
		appInputManager->getInput(appRenderSystem->newWindow, deltaTime, pauseTimer);

		update();

		appRenderSystem->render(appTimer);
		
		glfwSwapBuffers(appRenderSystem->newWindow);

		glfwPollEvents();
	}

}

void AppManager::update()
{
	appRenderSystem->updateMembers(appTimer);
}