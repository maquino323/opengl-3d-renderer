#include "AppManager.h"

int main(int argc, char **argv)
{
	AppManager newManager;

	newManager.initializeProgram();
	newManager.runMainLoop();
	

	return 0;
}
