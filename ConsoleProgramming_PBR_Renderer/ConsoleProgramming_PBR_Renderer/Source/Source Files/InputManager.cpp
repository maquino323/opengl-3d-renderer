#include "InputManager.h"

/*
// scroll callback for zooming camera in and out
 void InputManager::scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	renderSystem.sceneCamera.ProcessMouseScroll((float)yoffset);
}

// grabs mouse position
 void InputManager::mouse_callback(GLFWwindow* window, double xPos, double yPos)
{
	xOffset = (float)xPos - lastXPos;
	yOffset = (float)yPos - lastYPos;

	lastXPos = (float)xPos;
	lastYPos = (float)yPos;
	renderSystem.sceneCamera.ProcessMouseMovement(xOffset, yOffset);
}
*/
InputManager::InputManager()
{
	exitProgram = false;
};

InputManager::InputManager(RenderSystem* _renderSystem)
{
	renderSystem = _renderSystem;
	glfwSetInputMode(renderSystem->newWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // enable unlimited mouse motion
}

InputManager::~InputManager()
{

};


// grab input from keyboard
void InputManager::getInput(GLFWwindow* window, float deltaTime, bool &pause)
{

#pragma region CAMERA_MOVEMENT

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		renderSystem->sceneCamera.ProcessKeyboard(FORWARD, deltaTime);
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		renderSystem->sceneCamera.ProcessKeyboard(BACKWARD, deltaTime);
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		renderSystem->sceneCamera.ProcessKeyboard(RIGHT, deltaTime);
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		renderSystem->sceneCamera.ProcessKeyboard(LEFT, deltaTime);
	}

	// Move up
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
		renderSystem->sceneCamera.ProcessKeyboard(UP, deltaTime);
	}
	// Move down
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
		renderSystem->sceneCamera.ProcessKeyboard(DOWN, deltaTime);
	}

	// rotate camera
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		renderSystem->sceneCamera.Pitch -= 0.25f;
		renderSystem->sceneCamera.updateCameraVectors();
	}

	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		renderSystem->sceneCamera.Pitch += 0.25f;
		renderSystem->sceneCamera.updateCameraVectors();
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		renderSystem->sceneCamera.Yaw -= 0.25f;
		renderSystem->sceneCamera.updateCameraVectors();
	}

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		renderSystem->sceneCamera.Yaw += 0.25f;
		renderSystem->sceneCamera.updateCameraVectors();
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		renderSystem->sceneCamera.MovementSpeed = 7.5f;
	else
		renderSystem->sceneCamera.MovementSpeed = 2.5f;

#pragma endregion;

	// enable the skybox
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS)
	{
		renderSystem->useSkybox = !renderSystem->useSkybox;
		if (renderSystem->useSkybox)
			cout << "Using skybox" << endl;
		else
			cout << "Not using skybox" << endl;
	}

	// toggle text
	if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS)
	{
		renderSystem->drawText = !renderSystem->drawText;
	}	
	
	// toggle tonemapping
	if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS)
	{
		renderSystem->useTonemapping = !renderSystem->useTonemapping;
	}

	// reload shaders
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		renderSystem->shadersReloading = true;
		renderSystem->reloadShaders();
		renderSystem->shadersReloading = false;
	}

	// toggle wireframe
	if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
	{
		renderSystem->drawWireFrame = !renderSystem->drawWireFrame;
	}

	// draw lights
	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
	{
		renderSystem->drawLights = !renderSystem->drawLights;
	}

	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		pause = !pause;
	}

	if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS)
	{
		renderSystem->num--;

		if (renderSystem->num < 0)
			renderSystem->num = 9;
	}
	
	if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
	{
		renderSystem->num++;
		if (renderSystem->num > 9)
			renderSystem->num = 0;
	}



#pragma region RENDERING_PATH

	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
	{
		renderSystem->scene = 1;
		renderSystem->sceneCamera.Position = vec3(0.0f, 1.0f, 3.0f);
	}
	
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		renderSystem->scene = 2;
		renderSystem->sceneCamera.Position = vec3(0.0f, 2.0f, 30.0f);
	}

#pragma endregion

	// switch between render targets
#pragma region RENDER_TARGETS

	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
	{
		renderSystem->renderTarget = 0;
		renderSystem->target = renderSystem->renderTargets[0];
	}

	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
	{
		renderSystem->renderTarget = 1;
		renderSystem->target = renderSystem->renderTargets[1];
	}

	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
	{
		renderSystem->renderTarget = 2;
		renderSystem->target = renderSystem->renderTargets[2];
	}

	if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
	{
		renderSystem->renderTarget = 3;
		renderSystem->target = renderSystem->compositeAttachment;
	}

	if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)
	{
		renderSystem->renderTarget = 4;
		//renderSystem->target = renderSystem->renderTargets[4];
		renderSystem->target = renderSystem->sceneDepth;
	}

#pragma endregion


	// escape the program
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		exitProgram = true;
	}
}

/*
float InputManager::getLastXPos()
{
	return lastXPos;
}

float InputManager::getLastYPos()
{
	return lastYPos;
}

void InputManager::setXPos(float newXPos)
{
	lastXPos = newXPos;
}

void InputManager::setYPos(float newYPos)
{
	lastYPos = newYPos;
}
*/

