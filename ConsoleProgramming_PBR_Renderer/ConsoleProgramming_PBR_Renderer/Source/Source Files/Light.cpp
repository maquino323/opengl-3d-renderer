#include "../Source/Header Files/Light.h"
using namespace std;


Light::Light()
{
	Position = vec3(0.0);
	Color = vec4(0.0);
	Intensity = 1.0;
}

Light::Light(vec3 position, vec4 color, float intensity, float radius)
{
	Position = position;
	Color = color;
	Intensity = intensity;
	Radius = radius;
}

Light::~Light()
{

}


void Light::SetLightPosition(vec3 newPos)
{
	Position = newPos;
}

void Light::SetLightColor(vec4 newColor)
{
	Color = newColor;
}

void Light::SetLightIntensity(float newIntensity)
{
	Intensity = newIntensity;
}

void Light::SetRadius(float newRadius)
{
	Radius = newRadius;
}

vec4 Light::ToggleLight(bool on)
{
	return (on == true) ? Color : vec4(0.0, 0.0, 0.0, 0.0);
}


vec3 Light::GetLightPosition()
{
	return Position;
}

vec4 Light::GetLightColor()
{
	return Color;
}

float Light::GetLightIntensity()
{
	return Intensity;
}

float Light::GetRadius()
{
	return Radius;
}