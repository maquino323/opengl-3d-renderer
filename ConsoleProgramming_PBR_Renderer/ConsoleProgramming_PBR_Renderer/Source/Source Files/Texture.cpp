#include "Texture.h"

#include "GL/glew.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace std;

//constructors
ObjectTexture::ObjectTexture()
{
	image = "";
	ID = 0;
}

ObjectTexture::ObjectTexture(string imageName, bool flipImage)
{
	image = imageName;

	ID = loadImage(imageName, flipImage);
}

// destructor
ObjectTexture::~ObjectTexture()
{
	
}

// accessors
string ObjectTexture::GetImageName()
{
	return image;
}


unsigned int ObjectTexture::GetID()
{
	return ID;
}

void ObjectTexture::ChangeID(unsigned int newID)
{
	ID = newID;
}

// mutators
unsigned int ObjectTexture::loadImage(string newImage, bool flipImage)
{
	stbi_set_flip_vertically_on_load(flipImage);
	int width, height, nrComponents;
	float* data = stbi_loadf(newImage.c_str(), &width, &height, &nrComponents, 0);

	unsigned int texture;

	if (data)
	{
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, data);


		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}

	else
	{
		cout << "Failed to load  image" << endl;
		texture = -1;
	}

	image = newImage;
	return texture;
}