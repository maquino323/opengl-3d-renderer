#include "RenderSystem.h"

#include "glm/gtc/type_ptr.hpp"
#include <ctime>

using namespace std;

// error callback
void show_glfw_error(int error, char const* description)
{
	cerr << "Error: " << description << endl;
}

// when user resizes the window, adjust accordingly
void window_resized(GLFWwindow* window, int width, int height)
{
	glClearColor(0, 0, 0, 1);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);
	glfwSwapBuffers(window);
	//RenderSystem::updateScreenSize(width, height);
}

// ====================== PUBLIC ===================================
RenderSystem::RenderSystem()
{
	screen_width = 1024;
	screen_height = 728;
	appTimer = 0.0f;

	// seed random number generator
	srand(static_cast<unsigned> (time(0)));

	// initialize GLFW/GLEW, create window, set callbacks
	createGLFWContext();

	// initialize library for writing text 
	initTextLibrary();

	// Create VAOs/VBOs
	generateVertexObjects();

	// set up graphics settings
	setGraphicsSettings();

	// set up shader pipelines
	createShaders();

	// create framebuffers for rendering to screen
	createFramebuffer();

	// create lights and cameras, load in models, initialize matrices 
	createScene();

	// create textures to sample from in shaders
	createTextures();


}

RenderSystem::~RenderSystem()
{
	glfwDestroyWindow(newWindow);
}

int RenderSystem::render(float sceneTime)
{

	// render the skybox if enabled
	/*
	if (useSkybox)
	{
		glCullFace(GL_FRONT);
		skybox.use();
		skybox.setMat4("projection", projection);
		skybox.setMat4("view", mat4(mat3(view)));
		DrawCube();

		// reset culling
		glCullFace(GL_BACK);
	}
	*/

	
	if (drawWireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	

	// DEFERRED RENDERING
	// render positions, normals, depth data to G-buffer
	// then perform lighting calculations AT THE VERY END
	
	// we don't want to render while the shaders are reloading
	if (shadersReloading == false)
	{
		if (scene == 1)
		{

			// FIRST PASS = GEOMETRY
			// bind our new scene framebuffer
			// render scene to framebuffer

			glBindFramebuffer(GL_FRAMEBUFFER, sceneFramebuffer);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);

			modelDeferredShader.use();
			modelDeferredShader.setMat4("view", view);
			modelDeferredShader.setMat4("projection", projection);
			modelDeferredShader.setVec3("cameraPosition", sceneCamera.Position);
			modelDeferredShader.setVec3("viewDirection", sceneCamera.Position + sceneCamera.Front);


			// set sampler locations for model shader
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, planeTexture.GetID());
			glBindSampler(1, planeTexture.GetID());


			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, modelNormalMap.GetID());
			glBindSampler(3, modelNormalMap.GetID());

			modelDeferredShader.setInt("texture_diffuse", 1);
			modelDeferredShader.setInt("texture_normal", 3);

			// draw ground plane
			model = mat4(1.0f);
			//model *= scale(vec3(10.0f, 0.0f, 10.0f));
			modelDeferredShader.setMat4("model", model);
			glBindVertexArray(planeVAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);

			// SECOND PASS - DEFERRED LIGHTING
			// use G-buffer information to perform lighting on scene

#pragma region SECOND_PASS

		// SECOND PASS = LIGHTING
		// using g-buffer data, perform lighting calculations
			glBindFramebuffer(GL_FRAMEBUFFER, sceneCompositeBuffer);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			deferredLightingShader.use();

			glActiveTexture(GL_TEXTURE5);
			glBindTexture(GL_TEXTURE_2D, renderTargets[1]);

			glActiveTexture(GL_TEXTURE6);
			glBindTexture(GL_TEXTURE_2D, renderTargets[2]);

			glActiveTexture(GL_TEXTURE7);
			glBindTexture(GL_TEXTURE_2D, renderTargets[0]);


			deferredLightingShader.setInt("gPosition", 5);
			deferredLightingShader.setInt("gNormals", 6);
			deferredLightingShader.setInt("gAlbedo", 7);
			deferredLightingShader.setVec3("cameraPosition", sceneCamera.Position);
			deferredLightingShader.setFloat("numLights", numberOfLights[num]);


			// create the max size arrays
			vec3 lightColors[500];
			vec3 lightPositions[500];
			float lightRadii[500];
			float lightIntensities[500];
			for (int i = 0; i < numberOfLights[num]; ++i) // only fill a select number
			{
				lightColors[i] = deferredLights[i].GetLightColor();
				lightPositions[i] = deferredLights[i].GetLightPosition();
				lightRadii[i] = deferredLights[i].GetRadius();
				lightIntensities[i] = deferredLights[i].GetLightIntensity();// *abs((sin(sceneTime)));
			}

			// set arrays within the shaders
			glUniform3fv(glGetUniformLocation(deferredLightingShader.shaderID, "lightColors"), numberOfLights[num], value_ptr(lightColors[0]));
			glUniform3fv(glGetUniformLocation(deferredLightingShader.shaderID, "lightPositions"), numberOfLights[num], value_ptr(lightPositions[0]));
			glUniform1fv(glGetUniformLocation(deferredLightingShader.shaderID, "lightRadii"), numberOfLights[num], lightRadii);
			glUniform1fv(glGetUniformLocation(deferredLightingShader.shaderID, "lightIntensities"), numberOfLights[num], lightIntensities);
			deferredLightingShader.setBool("useTonemapping", useTonemapping);

			glBindVertexArray(fsqVAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);

			glBindTexture(GL_TEXTURE_2D, 0); // reset the texture for text rendering

#pragma endregion 


#pragma region THIRD_PASS

		// FINAL PASS - PRESENT
		// blit final result to default framebuffer
			glDisable(GL_DEPTH_TEST);
			
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			//glBindFramebuffer(GL_READ_FRAMEBUFFER, sceneFramebuffer);
			//glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			//glBlitFramebuffer(0, 0, screen_width, screen_height, 0, 0, screen_width, screen_height, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
			//glBindFramebuffer(GL_FRAMEBUFFER, 0);

			quadShader.use();
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, target);
			glBindSampler(2, target);
			quadShader.setInt("screenTexture", 2);

			glBindVertexArray(fsqVAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);

			glBindVertexArray(0);
			glBindTexture(GL_TEXTURE_2D, 0);

			// draw the scene lights over other geometry
			if (drawLights)
			{
				glEnable(GL_DEPTH_TEST);

				lightShader.use();
				lightShader.setMat4("view", view);
				lightShader.setMat4("projection", projection);

				for (int i = 0; i < numberOfLights[num]; ++i)
				{
					model = mat4(1.0f);
					//model *= scale(model, vec3(deferredLights[i].GetRadius()));
					model *= translate(model, vec3(deferredLights[i].GetLightPosition()));
					lightShader.setMat4("model", model);
					lightShader.setBool("useTonemapping", useTonemapping);
					lightShader.setVec3("color", deferredLights[i].GetLightColor() * deferredLights[i].GetLightIntensity());// *abs((sin(sceneTime)))));
					DrawSphere();
				}
			}

			// render scene controls over everything 
			if (drawText)
			{
				glDisable(GL_DEPTH_TEST);

				if (drawWireFrame)
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

				RenderControls();
			}

			// we don't want to accidentally begin forward rendering, so we can end here
			return 0;
		}


		// FORWARD RENDERING
		// supply all information to the fragment shader and perform lighting calculations per-fragment
		else if (scene == 2)
		{
			// we write to the default framebuffer
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);

			// we can set matrix values outside of paths since these will stay the same between the two
			model = mat4(1.0f);
			model = scale(vec3(10.0, 10.0, 10.0));
			model *= rotate(radians(180.0f), vec3(0.0, 0.0, 1.0));
			model *= rotate(radians(90.0f), vec3(1.0, 0.0, 0.0));
			model *= rotate(radians(45.0f), vec3(0.0, 0.0, 1.0));

			// set shader variables
			modelForwardShader.use();
			modelForwardShader.setMat4("model", model);
			modelForwardShader.setMat4("view", view);
			modelForwardShader.setMat4("projection", projection);
			modelForwardShader.setVec3("cameraPosition", sceneCamera.Position);
			modelForwardShader.setVec3("viewDirection", sceneCamera.Position + sceneCamera.Front);
			modelForwardShader.setFloat("displacementFactor", 0.0f);
			modelForwardShader.setMat4("viewProjection", projection * view);


			// set sampler locations for model shader
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, modelTexture.GetID());
			glBindSampler(1, modelTexture.GetID());

			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, modelNormalMap.GetID());
			glBindSampler(3, modelNormalMap.GetID());

			glActiveTexture(GL_TEXTURE4);
			glBindTexture(GL_TEXTURE_2D, modelDispMap.GetID());
			glBindSampler(4, modelDispMap.GetID());

			modelForwardShader.setInt("texture_diffuse", 1);
			modelForwardShader.setInt("texture_normal", 3);
			modelForwardShader.setInt("displacementMap", 4);

			vec3 lightColors[5];
			vec3 lightPositions[5];
			float lightIntensities[5];
			float lightRadii[5];
			for (int i = 0; i < 5; ++i)
			{
				lightColors[i] = forwardlights[i].GetLightColor();
				lightPositions[i] = forwardlights[i].GetLightPosition();
				lightIntensities[i] = forwardlights[i].GetLightIntensity();
				lightRadii[i] = forwardlights[i].GetRadius();
			}

			modelForwardShader.use();
			// set arrays within the shaders
			glUniform3fv(glGetUniformLocation(modelForwardShader.shaderID, "lightColors"), 5, value_ptr(lightColors[0]));
			glUniform3fv(glGetUniformLocation(modelForwardShader.shaderID, "lightPositions"), 5, value_ptr(lightPositions[0]));
			glUniform1fv(glGetUniformLocation(modelForwardShader.shaderID, "lightIntensities"), 5, lightIntensities);
			glUniform1fv(glGetUniformLocation(modelForwardShader.shaderID, "lightRadii"), 5, lightRadii);

			// draw models
			loadedModel.Draw(modelForwardShader);

			// draw the scene lights over other geometry
			if (drawLights)
			{
				lightShader.use();
				lightShader.setMat4("view", view);
				lightShader.setMat4("projection", projection);

				for (int i = 0; i < 5; ++i)
				{
					model = mat4(1.0f);
					model *= translate(model, vec3(forwardlights[i].GetLightPosition()));
					//model *= scale(model, vec3(0.01, 0.01, 0.01));
					lightShader.setMat4("model", model);
					lightShader.setVec3("color", forwardlights[i].GetLightColor());
					DrawSphere();
				}
			}

			// render scene controls over everything 
			if (drawText)
			{
				if (drawWireFrame)
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

				RenderControls();
			}

			return 0;
		}

	}
	// if we did not render anything
	return -1;
	

#pragma endregion
}

void RenderSystem::updateMembers(float sceneTime)
{
	projection = perspective(radians(sceneCamera.Zoom), (float)screen_width / (float)screen_height, 0.1f, 100.f);
	view = sceneCamera.GetViewMatrix();
	//lights[2].SetLightPosition(vec3(sin(appTimer), cos(appTimer), 0.0f));
}



// ===================== PRIVATE ===================================

void RenderSystem::generateVertexObjects()
{
	// create VAO/VBO for text
	glGenVertexArrays(1, &textVAO);
	glGenBuffers(1, &textVBO);
	glBindVertexArray(textVAO);
	glBindBuffer(GL_ARRAY_BUFFER, textVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// create VAO/VBO for full screen quad
	glGenVertexArrays(1, &fsqVAO);
	glGenBuffers(1, &fsqVBO);
	glBindVertexArray(fsqVAO);
	glBindBuffer(GL_ARRAY_BUFFER, fsqVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// create VAO/VBO/EBO for ground plane
	glGenVertexArrays(1, &planeVAO);
	glGenBuffers(1, &planeVBO);
	glGenBuffers(1, &planeEBO);
	glBindVertexArray(planeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), &planeVertices, GL_STATIC_DRAW);

	// positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);

	// normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));

	// texcoords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// create VAO/VBO for cube
	glGenVertexArrays(1, &cubeVAO);
	glBindVertexArray(cubeVAO);
	glGenBuffers(1, &cubeVBO);
	glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertexAttributes), cubeVertexAttributes, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

// create FBO for cubemap
void RenderSystem::createFramebuffer()
{
	// generate the framebuffer and renderbuffer
	glGenFramebuffers(1, &sceneFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, sceneFramebuffer);


	// depth needs to be its own texture, so we create this outside of the loop and add it after
	for (int i = 0; i < 4; ++i)
	{
		glGenTextures(1, &renderTargets[i]);
		glBindTexture(GL_TEXTURE_2D, renderTargets[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, screen_width, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glBindTexture(GL_TEXTURE_2D, 0);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, renderTargets[i], 0);
	}
	
	glGenTextures(1, &sceneDepth);
	glBindTexture(GL_TEXTURE_2D, sceneDepth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, screen_width, screen_height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, sceneDepth, 0);

	renderTargets[4] = sceneDepth;

	// we MUST tell OpenGL that we would like to use multiple render targets
	GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2,
							GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5 };
	glDrawBuffers(5, buffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cout << "Incomplete framebuffer...failed to create scene framebuffer object" << endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// composite framebuffer
	glGenFramebuffers(1, &sceneCompositeBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, sceneCompositeBuffer);

	glGenTextures(1, &compositeAttachment);
	glBindTexture(GL_TEXTURE_2D, compositeAttachment);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, screen_width, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, compositeAttachment, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	target = compositeAttachment;  // when we go into deferred rendering, we want this to be the first thing we see
}



void RenderSystem::createCubemap(GLuint* texture, int x, int y)
{
	// create the cubemap for the camera skybox
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texture);

	for (unsigned int i = 0; i < 6; ++i)
	{
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, x, y, 0, GL_RGB, GL_FLOAT, nullptr);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

void RenderSystem::DrawCube()
{
	if (cubeVAO == 0)
	{
		glGenVertexArrays(1, &cubeVAO);
		glGenBuffers(1, &cubeVBO);

		// fill the buffer
		glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertexAttributes), cubeVertexAttributes, GL_STATIC_DRAW);

		// link vertex attributes
		glBindVertexArray(cubeVAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	glBindVertexArray(cubeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void RenderSystem::DrawSphere()
{
	if (sphereVAO == 0)
	{
		glGenVertexArrays(1, &sphereVAO);

		unsigned int vbo, ebo;
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ebo);

		vector<vec3> positions;
		vector<vec2> uv;
		vector<vec3> normals;
		vector<unsigned int> indices;

		const unsigned int X_SEGMENTS = 64;
		const unsigned int Y_SEGMENTS = 64;
		const float PI = 3.14159265359f;
		for (unsigned int y = 0; y <= Y_SEGMENTS; ++y)
		{
			for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
			{
				float xSegment = (float)x / (float)X_SEGMENTS;
				float ySegment = (float)y / (float)Y_SEGMENTS;
				float xPos = cos(xSegment * 2.0f * PI) * sin(ySegment * PI);
				float yPos = cos(ySegment * PI);
				float zPos = sin(xSegment * 2.0f * PI) * sin(ySegment * PI);

				vec3 pos = vec3(xPos, yPos, zPos);
				positions.push_back(pos);
				uv.push_back(vec2(xSegment, ySegment));
				normals.push_back(pos);
			}
		}

		bool oddRow = false;
		for (int y = 0; y < Y_SEGMENTS; ++y)
		{
			if (!oddRow) // even rows: y == 0, y == 2; and so on
			{
				for (int x = 0; x <= X_SEGMENTS; ++x)
				{
					indices.push_back(y * (X_SEGMENTS + 1) + x);
					indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
				}
			}
			else
			{
				for (int x = X_SEGMENTS; x >= 0; --x)
				{
					indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
					indices.push_back(y * (X_SEGMENTS + 1) + x);
				}
			}
			oddRow = !oddRow;
		}
		indexCount = indices.size();


		vector<float> data;
		for (int i = 0; i < (int)positions.size(); ++i)
		{
			data.push_back(positions[i].x);
			data.push_back(positions[i].y);
			data.push_back(positions[i].z);
			if (uv.size() > 0)
			{
				data.push_back(uv[i].x);
				data.push_back(uv[i].y);
			}
			if (normals.size() > 0)
			{
				data.push_back(normals[i].x);
				data.push_back(normals[i].y);
				data.push_back(normals[i].z);
			}
		}
		glBindVertexArray(sphereVAO);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

		GLsizei stride = (3 + 2 + 3) * sizeof(float);
		glEnableVertexAttribArray(0); // positions
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)0);
		glEnableVertexAttribArray(1); // normals
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2); // texcoords
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride, (void*)(5 * sizeof(float)));


		glBindVertexArray(0);
	}

	glBindVertexArray(sphereVAO);
	glDrawElements(GL_TRIANGLE_STRIP, indexCount, GL_UNSIGNED_INT, 0);
}

void RenderSystem::RenderControls()
{
	float startX = 5.0f;
	float startY = screen_height - 145.0f;

	string wire = (drawWireFrame == true) ? "off" : "on";

	// default text, displays controls
	DrawText(textShader, "Use WASD to move the camera.", startX, startY, 0.5, colors[9]);
	DrawText(textShader, "Hold Left Shift for faster speed.", startX, startY - 25.0f, 0.5, colors[9]);
	DrawText(textShader, "Press F to turn wireframe " + wire, startX, startY - 50.0f, 0.5, colors[9]);
	DrawText(textShader, "Press T to toggle text.", startX, startY - 75.0f, 0.5, colors[9]);
	DrawText(textShader, "Press ESC to Exit!", startX, 15.0f, 0.5, colors[9]);


	string currentTarget = "";
	string tonemapping = (useTonemapping) ? "off" : "on";

	switch (scene)
	{
		// forward rendering
	case 2:
		DrawText(textShader, "Current rendering path: forward", startX, startY - 100.0f, 0.5, colors[9]);
		DrawText(textShader, "Number of lights: " + to_string(sizeof(forwardlights) / sizeof(Light)), startX, startY - 125.0f, 0.5, colors[9]);
		break;
		
		// deferred rendering
	case 1:
		
		DrawText(textShader, "Current rendering path: deferred", startX, startY - 100.0f, 0.5, colors[9]);
		DrawText(textShader, "Press 1 - 5 to switch between render targets.", startX, startY - 125.0f, 0.5, colors[9]);

		// find out which render target we're currently looking at
		switch (renderTarget)
		{
		case 0:
			currentTarget = "albedo";
			break;

		case 1:
			currentTarget = "positions";
			break;

		case 2:
			currentTarget = "normals";
			break;

		case 3:
			currentTarget = "composite";
			break;

		case 4:
			currentTarget = "depth";
			break;

		default:
			currentTarget = "nothing? something's wrong...";
			break;
		}

		DrawText(textShader, "Current render target: " + currentTarget, startX, startY - 150.0f, 0.5, colors[9]);
		DrawText(textShader, "Number of lights: " + to_string(numberOfLights[num]), startX, startY - 175.0f, 0.5, colors[9]);

		DrawText(textShader, "Press Y to turn tonemapping " + tonemapping, startX, startY - 200.0f, 0.5, colors[9]);
		


		break;

		// nothing?
	default :

		break;
	}


	
}

void RenderSystem::DrawText(Shader& shader, string text, float startX, float startY, float scale, vec3 textColor)
{
	// Activate corresponding render state	
	shader.use();
	shader.setMat4("projection", textProj);
	shader.setVec3("textColor", textColor);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(textVAO);

	// Iterate through all characters
	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++)
	{
		Character ch = Characters[*c];

		GLfloat xpos = startX + ch.Bearing.x * scale;
		GLfloat ypos = startY - (ch.Size.y - ch.Bearing.y) * scale;

		GLfloat w = ch.Size.x * scale;
		GLfloat h = ch.Size.y * scale;
		// Update VBO for each character
		GLfloat vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos,     ypos,       0.0, 1.0 },
			{ xpos + w, ypos,       1.0, 1.0 },

			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos + w, ypos,       1.0, 1.0 },
			{ xpos + w, ypos + h,   1.0, 0.0 }
		};
		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, textVBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		startX += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void RenderSystem::ChangeBackgroundImage(unsigned int textureID)
{
	/*
	equirectangularToCubemap.use();
	equirectangularToCubemap.setInt("equirectangularMap", 0);
	equirectangularToCubemap.setMat4("projection", captureProjection);
	model = mat4(1.0);
	equirectangularToCubemap.setMat4("model", model);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);

	glViewport(0, 0, 1024, 1024);
	glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
	for (unsigned int i = 0; i < 6; ++i)
	{
		equirectangularToCubemap.setMat4("view", captureView[i]);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, envCubeMap, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		DrawCube();
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	useSkybox = true;

	*/
}

void RenderSystem::initTextLibrary()
{
	// init text library
	if (FT_Init_FreeType(&ft))
	{
		cout << "Could not init freetype library..." << endl;
		getchar();
		exit(-1);
	}

	if (FT_New_Face(ft, FONTS"times.ttf", 0, &face))
	{
		cout << "Could not open font..." << endl;
		getchar();
		exit(-1);
	}

	FT_Set_Pixel_Sizes(face, 0, 48);

	g = face->glyph;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// only get the first 128 characters of the ASCII character set
	for (GLubyte c = 0; c < 128; c++)
	{
		// Load character glyph 
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}
		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Now store character for later use
		Character character = {
			texture,
			ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			face->glyph->advance.x
		};
		Characters.insert(std::pair<GLchar, Character>(c, character));
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ft);
}

/*
// ====================
void RenderSystem::updateScreenSize(int width, int height)
{
	screen_width = width;
	screen_height = height;
}
*/


void RenderSystem::setGraphicsSettings()
{
	glViewport(0, 0, screen_width, screen_height);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void RenderSystem::createShaders()
{
	// load shaders
	//sphereShader =				Shader(VERTEX"pbr_vertex_shader.glsl",				FRAGMENT"phong_fragment_shader.glsl");
	lightShader =				Shader(VERTEX"simple_vertex_shader.glsl",			FRAGMENT"simple_fragment_shader.glsl");
	textShader =				Shader(VERTEX"text_vertex_shader.glsl",				FRAGMENT"text_fragment_shader.glsl");


	modelDeferredShader =		Shader(VERTEX"model_deferred_vertex_shader.glsl",	FRAGMENT"model_deferred_fragment_shader.glsl"); // VERTEX -> FRAGMENT
	
	modelForwardShader =		Shader(VERTEX"model_forward_vertex_shader.glsl", GEOMETRY"model_geometry_shader.glsl",  // VS -> TCS -> TES -> GS -> FS
										TESSELLATION"model_control_shader.glsl", TESSELLATION"model_evaluation_shader.glsl",
											FRAGMENT"model_forward_fragment_shader.glsl");

	//cubeShader =				Shader(VERTEX"cube_vertex_shader.glsl",				FRAGMENT"cube_fragment_shader.glsl");

	quadShader =				Shader(VERTEX"fsq_vertex_shader.glsl",				FRAGMENT"fsq_fragment_shader.glsl");
	deferredLightingShader =			Shader(VERTEX"deferred_lighting_vertex_shader.glsl",FRAGMENT"deferred_lighting_fragment_shader.glsl");
	
	planeShader = Shader(VERTEX"plane_vertex_shader.glsl", FRAGMENT"plane_fragment_shader.glsl");

	// patch parameters for tessellation control shader
	glPatchParameteri(GL_PATCH_VERTICES, 3);
}

void RenderSystem::createGLFWContext()
{
	glfwSetErrorCallback(show_glfw_error);

	if (!glfwInit())
	{
		cerr << "GLFW failed to initialize...Terminating program!" << endl;
		exit(-1);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);


	// create a new window
	newWindow = glfwCreateWindow(screen_width, screen_height, "OpenGL Renderer", NULL, NULL);
	glfwMakeContextCurrent(newWindow);

	if (!newWindow)
	{
		cerr << "Error: failed to create new window... Terminating program" << endl;
		glfwTerminate();
		exit(-1);
	}

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	// initialize GLEW
	if (err != GLEW_OK)
	{
		cerr << "GLEW failed to initialize: " << glewGetErrorString(err) << endl;
		exit(-1);
	}

	glfwSetWindowSizeCallback(newWindow, window_resized);
}

void RenderSystem::createScene()
{
	sceneCamera = Camera(vec3(0.0f, 2.0f, 30.0f));

	// orthographic projection matrix for rendering text to screen
	//textProj = ortho(0.0f, (float)screen_width, 0.0f, (float)screen_height);
	textProj = ortho(0.0f, 800.0f, 0.0f, 600.0f);

	// forward rendering lights
	//				  position							color					intensity	radius
	forwardlights[0] = Light(vec3(-2, 6, -5),			vec4(colors[6], 1.0),	1.0f,		1.0f); // torches are orange
	forwardlights[1] = Light(vec3(6, 6, 4),				vec4(colors[6], 1.0),	1.0f,		1.0f);
	forwardlights[2] = Light(vec3(-0.75, 2, 1),			vec4(colors[0], 1.0),	0.5f,		1.0f); // firepit is red
	forwardlights[3] = Light(vec3(0.75, 2.5, -2.75),	vec4(colors[6], 1.0),	1.0f,		1.0f); // candles
	forwardlights[4] = Light(vec3(3, 2.5, -0.5),		vec4(colors[6], 1.0),	1.0f,		1.0f);


	//deferredLights[0] = Light(vec3(-1.0f, 1.0f, -1.0f), vec4(colors[0], 1.0),  1.0f,		0.25f); 
	//deferredLights[1] = Light(vec3(1.0f,  1.0f, -1.0f), vec4(colors[1], 1.0),  1.0f,		0.25f);
	//deferredLights[2] = Light(vec3(-1.0f, 1.0f, 1.0f),  vec4(colors[2], 1.0),  1.0f,		0.25f); 
	//deferredLights[3] = Light(vec3(1.0f,  1.0f, 1.0f),  vec4(colors[3], 1.0),  1.0f,		0.25f); 

	// create pool of lights
	for (int i = 0; i < 500; ++i)
	{
		float x = rand() % 18 - 9; // random value between -9 and 9
		float z = rand() % 18 - 9; // random value between -9 and 9

		int randColor = rand() % 9;
		vec4 lightColor = vec4(colors[randColor], 1.0f);

		float radius = 0.25f; // default value

		//float intensity = 1.0f; // default value
		float intensity = (float)(rand() % 4) + 1; // random value between 1 and 5

		deferredLights[i] = Light(vec3(x, 1.0f, z), lightColor, intensity, radius);

	}

	loadedModel = Model(MODELS"viking_room.obj", false);
	//loadedModel = Model(MODELS"Runway.obj", false);
	modelLoaded = loadedModel.isLoaded;
}

void RenderSystem::createTextures()
{
	modelTexture =		ObjectTexture(TEXTURES"viking_room.png", false);
	modelSpecularMap =	ObjectTexture(TEXTURES"viking_specular_map.png", false);
	modelNormalMap =	ObjectTexture(TEXTURES"viking_normal_map.png", false);
	modelDispMap =		ObjectTexture(TEXTURES"viking_displacement_map.png", false);
	planeTexture =		ObjectTexture(TEXTURES"brickwall.jpg", true);

}


void RenderSystem::reloadShaders()
{
	deferredLightingShader.reloadShaders();
}


int  RenderSystem::getScreenWidth() { return screen_width; }

int RenderSystem::getScreenHeight() { return screen_height; }

Camera RenderSystem::getCamera() { return sceneCamera; }