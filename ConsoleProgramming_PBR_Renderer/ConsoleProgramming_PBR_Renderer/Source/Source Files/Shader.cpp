#include "Shader.h"

Shader::Shader()
{

}

Shader::Shader(const char* vertexShaderPath, const char* fragmentShaderPath)
{
	string vertexCode, fragCode;
	ifstream vertShader, fragShader;

	vertShader.exceptions(ifstream::failbit | ifstream::badbit);
	fragShader.exceptions(ifstream::failbit | ifstream::badbit);

	try
	{
		vertShader.open(vertexShaderPath);
		fragShader.open(fragmentShaderPath);

		stringstream vertStream, fragStream;
		vertStream << vertShader.rdbuf();
		fragStream << fragShader.rdbuf();

		vertShader.close();
		fragShader.close();

		vertexCode = vertStream.str();
		fragCode = fragStream.str();
	}
	catch (ifstream::failure e)
	{
		cout << "ERROR: SHADER::FILE NOT SUCCESSFULLY READ" << endl;
	}

	const char* vShaderCode  = vertexCode.c_str();
	const char* fShaderCode = fragCode.c_str();

	unsigned int vert, frag;
	v = vert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vert, 1, &vShaderCode, NULL);
	glCompileShader(vert);
	checkCompileErrors(vert, "VERTEX");

	f = frag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(frag, 1, &fShaderCode, NULL);
	glCompileShader(frag);
	checkCompileErrors(frag, "FRAGMENT");

	shaderID = glCreateProgram();
	glAttachShader(shaderID, vert);
	glAttachShader(shaderID, frag);
	glLinkProgram(shaderID);
	checkCompileErrors(shaderID, "PROGRAM");

	vsPath = vertexShaderPath;
	fsPath = fragmentShaderPath;

	glDeleteShader(vert);
	glDeleteShader(frag);
}

Shader::Shader(const char* vertexShaderPath, const char* geometryShaderPath, const char* fragmentShaderPath)
{
	string vertexCode, geomCode, fragCode;
	ifstream vertShader, geomShader, fragShader;

	vertShader.exceptions(ifstream::failbit | ifstream::badbit);
	geomShader.exceptions(ifstream::failbit | ifstream::badbit);
	fragShader.exceptions(ifstream::failbit | ifstream::badbit);

	try
	{
		vertShader.open(vertexShaderPath);
		geomShader.open(geometryShaderPath);
		fragShader.open(fragmentShaderPath);

		stringstream vertStream, geomStream, fragStream;
		vertStream << vertShader.rdbuf();
		vertShader.close();

		geomStream << geomShader.rdbuf();
		geomShader.close();
		
		fragStream << fragShader.rdbuf();
		fragShader.close();


		vertexCode = vertStream.str();
		geomCode = geomStream.str();
		fragCode = fragStream.str();
	}
	catch (ifstream::failure e)
	{
		cout << "ERROR: SHADER::FILE NOT SUCCESSFULLY READ" << endl;
	}

	const char* vShaderCode  = vertexCode.c_str();
	const char* gShaderCode  = geomCode.c_str();
	const char* fShaderCode = fragCode.c_str();

	unsigned int vert, geom, frag;
	v = vert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vert, 1, &vShaderCode, NULL);
	glCompileShader(vert);
	checkCompileErrors(vert, "VERTEX");
	
	g = geom = glCreateShader(GL_GEOMETRY_SHADER);
	glShaderSource(geom, 1, &gShaderCode, NULL);
	glCompileShader(geom);
	checkCompileErrors(geom, "GEOMETRY");

	f = frag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(frag, 1, &fShaderCode, NULL);
	glCompileShader(frag);
	checkCompileErrors(frag, "FRAGMENT");

	shaderID = glCreateProgram();
	glAttachShader(shaderID, vert);
	glAttachShader(shaderID, frag);
	glAttachShader(shaderID, geom);
	glLinkProgram(shaderID);
	checkCompileErrors(shaderID, "PROGRAM");

	vsPath = vertexShaderPath;
	gsPath = geometryShaderPath;
	fsPath = fragmentShaderPath;

	glDeleteShader(vert);
	glDeleteShader(geom);
	glDeleteShader(frag);
}

Shader::Shader(const char* vertexShaderPath, const char* geometryShaderPath, const char* tessControlShaderPath, const char* tessEvalShaderPath, const char* fragmentShaderPath)
{
	string vertexCode, geomCode, controlCode, evalCode, fragCode;
	ifstream vertShader, geomShader, controlShader, evalShader, fragShader;

	vertShader.exceptions(ifstream::failbit | ifstream::badbit);
	geomShader.exceptions(ifstream::failbit | ifstream::badbit);
	fragShader.exceptions(ifstream::failbit | ifstream::badbit);
	controlShader.exceptions(ifstream::failbit | ifstream::badbit);
	evalShader.exceptions(ifstream::failbit | ifstream::badbit);

	try
	{
		vertShader.open(vertexShaderPath);
		geomShader.open(geometryShaderPath);
		fragShader.open(fragmentShaderPath);
		controlShader.open(tessControlShaderPath);
		evalShader.open(tessEvalShaderPath);

		stringstream vertStream, geomStream, evalStream, controlStream, fragStream;
		vertStream << vertShader.rdbuf();
		vertShader.close();

		geomStream << geomShader.rdbuf();
		geomShader.close();
		
		fragStream << fragShader.rdbuf();
		fragShader.close();
		
		controlStream << controlShader.rdbuf();
		controlShader.close();
		
		evalStream << evalShader.rdbuf();
		evalShader.close();


		vertexCode = vertStream.str();
		geomCode = geomStream.str();
		fragCode = fragStream.str();
		controlCode = controlStream.str();
		evalCode = evalStream.str();
	}
	catch (ifstream::failure e)
	{
		cout << "ERROR: SHADER::FILE NOT SUCCESSFULLY READ" << endl;
	}

	const char* vShaderCode  = vertexCode.c_str();
	const char* gShaderCode  = geomCode.c_str();
	const char* fShaderCode = fragCode.c_str();
	const char* cShaderCode = controlCode.c_str();
	const char* eShaderCode = evalCode.c_str();

	unsigned int vert, geom, frag, cont, eval;

	// VERTEX SHADER
	v = vert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vert, 1, &vShaderCode, NULL);
	glCompileShader(vert);
	checkCompileErrors(vert, "VERTEX");
	
	// GEOMETRY SHADER
	g = geom = glCreateShader(GL_GEOMETRY_SHADER);
	glShaderSource(geom, 1, &gShaderCode, NULL);
	glCompileShader(geom);
	checkCompileErrors(geom, "GEOMETRY");


	// FRAGMENT SHADER
	f = frag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(frag, 1, &fShaderCode, NULL);
	glCompileShader(frag);
	checkCompileErrors(frag, "FRAGMENT");

	// TESSELLATION CONTROL SHADER
	tc = cont = glCreateShader(GL_TESS_CONTROL_SHADER);
	glShaderSource(cont, 1, &cShaderCode, NULL);
	glCompileShader(cont);
	checkCompileErrors(cont, "TESS_CONTROL");


	// TESSELLATION EVALUATION SHADER
	te = eval = glCreateShader(GL_TESS_EVALUATION_SHADER);
	glShaderSource(eval, 1, &eShaderCode, NULL);
	glCompileShader(eval);
	checkCompileErrors(eval, "TESS_EVALUATION");

	shaderID = glCreateProgram();
	glAttachShader(shaderID, vert);
	glAttachShader(shaderID, frag);
	glAttachShader(shaderID, geom);
	glAttachShader(shaderID, cont);
	glAttachShader(shaderID, eval);
	glLinkProgram(shaderID);
	checkCompileErrors(shaderID, "PROGRAM");

	vsPath = vertexShaderPath;
	gsPath = geometryShaderPath;
	fsPath = fragmentShaderPath;
	tcPath = tessControlShaderPath;
	tePath = tessEvalShaderPath;

	glDeleteShader(vert);
	glDeleteShader(geom);
	glDeleteShader(frag);
	glDeleteShader(cont);
	glDeleteShader(eval);
}

Shader::~Shader()
{

}

void Shader::use() 
{ 
	glUseProgram(shaderID); 
};

void Shader::setBool(const string& name, bool value) const 
{ 
	glUniform1i(glGetUniformLocation(shaderID, name.c_str()), (int)value); 
}

void Shader::setInt(const string& name, int value) const 
{ 
	glUniform1i(glGetUniformLocation(shaderID, name.c_str()), value); 
}

void Shader::setFloat(const string& name, float value) const 
{ 
	glUniform1f(glGetUniformLocation(shaderID, name.c_str()), value); 
}

void Shader::setMat4(const string& name, mat4 matrix) const 
{ 
	glUniformMatrix4fv(glGetUniformLocation(shaderID, name.c_str()), 1, GL_FALSE, &matrix[0][0]);
}

void Shader::setVec3(const string& name, vec3 vect) const
{
	glUniform3f(glGetUniformLocation(shaderID, name.c_str()), vect.r, vect.g, vect.b);
}

void Shader::setVec4(const string& name, vec4 vect) const
{
	glUniform4f(glGetUniformLocation(shaderID, name.c_str()), vect.r, vect.g, vect.b, vect.a);
}


void Shader::checkCompileErrors(unsigned int shader, std::string type)
{
	int success;
	char infoLog[1024];
	if (type != "PROGRAM")
	{
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(shader, 1024, NULL, infoLog);
			std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
		}
	}
	else
	{
		glGetProgramiv(shader, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(shader, 1024, NULL, infoLog);
			std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
		}
	}
}

void Shader::reloadShaders()
{
	string vertexCode, fragCode;
	ifstream vertShader, fragShader;

	vertShader.exceptions(ifstream::failbit | ifstream::badbit);
	fragShader.exceptions(ifstream::failbit | ifstream::badbit);

	try
	{
		vertShader.open(vsPath);
		fragShader.open(fsPath);

		stringstream vertStream, fragStream;
		vertStream << vertShader.rdbuf();
		fragStream << fragShader.rdbuf();

		vertShader.close();
		fragShader.close();

		vertexCode = vertStream.str();
		fragCode = fragStream.str();
	}
	catch (ifstream::failure e)
	{
		cout << "ERROR: SHADER::FILE NOT SUCCESSFULLY READ" << endl;
	}
	const char* vShaderCode = vertexCode.c_str();
	const char* fShaderCode = fragCode.c_str();

	v = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(v, 1, &vShaderCode, NULL);
	glCompileShader(v);
	checkCompileErrors(v, "VERTEX");

	f = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(f, 1, &fShaderCode, NULL);
	glCompileShader(f);
	checkCompileErrors(f, "FRAGMENT");

	shaderID = glCreateProgram();
	glAttachShader(shaderID, v);
	glAttachShader(shaderID, f);
	glLinkProgram(shaderID);
	checkCompileErrors(shaderID, "PROGRAM");

}
